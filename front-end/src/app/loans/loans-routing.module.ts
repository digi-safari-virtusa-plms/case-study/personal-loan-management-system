import { CustomerloansComponent } from "./components/customerloans/customerloans.component";
import { ManagerDashboardComponent } from "./components/manager-dashboard/manager-dashboard.component";
import { NgModule, OnInit } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { CustomerDashboardComponent } from "./components/customer-dashboard/customer-dashboard.component";
import { LoansComponent } from "./loans.component";
import { LoansListComponent } from "./components/loans-list/loans-list.component";
import {  AuthGuard } from '../shared/guards/auth.guard';

const routes: Routes = [
  {
    path: "",
    component: LoansComponent,
    children: [
      {
        path: "customer",
        component: CustomerDashboardComponent,
        canActivate:[AuthGuard],
        children: [
          
          {
            path: "applied-loans",
            component: CustomerloansComponent,
            canActivate:[AuthGuard]
          },
          {
            path: "",
            redirectTo: "applied-loans",
            pathMatch: "full"
          }
        ]
      },
      {
        path: "loans-list",
        component: LoansListComponent,
        canActivate:[AuthGuard]
      },
      { path: "manager/dashboard", component: ManagerDashboardComponent , canActivate: [AuthGuard]}
      
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoansRoutingModule {}
