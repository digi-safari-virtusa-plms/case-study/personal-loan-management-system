export class Loan {
  public id: string;
  public bankName: string;
  public bankImageUrl: string;
  public minLoanAmount: number;
  public maxLoanAmount: number;
  public minInterestRate: number;
  public maxInterestRate: number;
  public minCreditScore: number;
  public termLength: number;
  public processingFee: number;
  public rating: number;
  public favouriteCount: number;

  constructor(){
    this.rating = 0;
    this.favouriteCount = 0;
  }
}
