import { LoansComponent } from "./loans.component";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { LoansRoutingModule } from "./loans-routing.module";
import { CustomerDashboardComponent } from "./components/customer-dashboard/customer-dashboard.component";
import { ManagerDashboardComponent } from "./components/manager-dashboard/manager-dashboard.component";
import { AngularMaterialModule } from "../angular-material/angular-material.module";
import { LoansListComponent } from "./components/loans-list/loans-list.component";
import { CustomerloansComponent } from "./components/customerloans/customerloans.component";
import {StarRatingModule} from 'angular-star-rating';
import { RatingModule } from 'ng-starrating';
import { ApplyLoanComponent } from './components/apply-loan/apply-loan.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    LoansComponent,
    CustomerloansComponent,
    CustomerDashboardComponent,
    ManagerDashboardComponent,
    LoansListComponent,
    CustomerloansComponent,    
    ApplyLoanComponent
  ],
  imports: [CommonModule, LoansRoutingModule, AngularMaterialModule,RatingModule,FormsModule,ReactiveFormsModule]
})
export class LoansModule {
  constructor() {
    console.log("Inside LoansModule");
  }
}
