import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormGroupDirective } from '@angular/forms';
import { LoginUser } from '../../models/loginuser.model';
import { AuthenticationService } from 'src/app/shared/services/authentication.service';
import { RouterService } from 'src/app/loans/services/router.service';
import { SessionStorageService } from 'src/app/shared/services/sessionstorage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  loginForm : FormGroup;
  loginUser : LoginUser;
  loginButtonText:string;
  errorMessage : string;

  @ViewChild(FormGroupDirective,{static:true})
  formGroupDirective : FormGroupDirective
  
  constructor(public formBuilder:FormBuilder,
              private authenticationService : AuthenticationService,
              private routerService : RouterService,
              private sessionStorageService : SessionStorageService) {
    this.loginButtonText = "Login";
    this.loginUser = new LoginUser();
    this.loginForm = this.formBuilder.group({
      email : ['',Validators.compose([Validators.required,Validators.email])],
      password : ['',Validators.compose([Validators.required,Validators.minLength(5)])]
  });
   }

  ngOnInit() {
  }

  /*
  Chane this code once you implement the backend services 
  Should get JWT token using AuthenticationService.validateUser(this.loginUser)
  along with user details
  */

  doCustomerLogin(loginForm: FormGroup){
    this.loginButtonText = "Validating...";
    this.loginUser = this.loginForm.value;
    this.authenticationService.validateUser(this.loginUser).then(response =>{
      console.log('Authenticated Customer', response);
      if(response !== null){        
        this.sessionStorageService.setSessionStorageData('id', response.id);
        this.sessionStorageService.setSessionStorageData('name',response.name);
        this.sessionStorageService.setSessionStorageData('role',response.role);
        this.sessionStorageService.setSessionStorageData('token',response.token);
        this.sessionStorageService.getSessionStorageData(); //calling this method to update the HeaderComponent with user data
        if(response.role === 'customer')
        this.routerService.routeToCustomerLoansList();
        else if(response.role === 'manager')
        this.routerService.routeToManagerDashbaord();
      }else if(response === null){
        console.log("Error Message");        
        this.errorMessage = "Invalid Credentials..";
        this.loginButtonText = "Login";
      }

      
    },error =>{
      console.log("Error Inside");
      
    })
    
  }

}
