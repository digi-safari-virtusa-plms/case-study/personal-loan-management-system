import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthenticationService } from 'src/app/shared/services/authentication.service';
import { SessionStorageService } from 'src/app/shared/services/sessionstorage.service';

@Injectable({
    providedIn: 'root'
  })
export class CustomerServiceHttpInterceptor implements HttpInterceptor{

    constructor(private sessionStorageService : SessionStorageService){
    }

    intercept(request : HttpRequest<any>, next: HttpHandler) : Observable<HttpEvent<any>>{
        request = request.clone({
            setHeaders: {
                Authorization : `Bearer ${this.sessionStorageService.getBearerToken()}`
            }
        });
        return next.handle(request);
    }

}