package com.digisafari.plms.customer.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code= HttpStatus.NOT_FOUND, reason = "Loan doesn't exist with this ID")
public class LoanNotFoundException extends Exception {

}
