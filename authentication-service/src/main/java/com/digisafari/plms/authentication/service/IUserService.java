package com.digisafari.plms.authentication.service;

import com.digisafari.plms.authentication.domain.User;
import com.digisafari.plms.authentication.exception.UserNotFoundException;

public interface IUserService {

	
	public User saveUser(User user);
	public User findByEmailAndPassword(String email, String password) throws UserNotFoundException;
	public User updateUser(User user) throws UserNotFoundException;
}
