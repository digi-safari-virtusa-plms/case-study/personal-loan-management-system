package com.digisafari.plms.authentication.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND , reason = "Invalid Email and Password")
public class UserNotFoundException extends Exception {

}
