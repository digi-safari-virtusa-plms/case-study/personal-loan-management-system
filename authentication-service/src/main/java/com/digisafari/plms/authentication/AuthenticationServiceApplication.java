package com.digisafari.plms.authentication;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import com.digisafari.plms.authentication.domain.User;
import com.digisafari.plms.authentication.repository.UserRepository;
import com.digisafari.plms.authentication.service.UserService;

@SpringBootApplication
@EnableEurekaClient
public class AuthenticationServiceApplication implements CommandLineRunner {

	@Autowired
	ResourceLoader resourceLoader;
	
	@Autowired
	UserService userService;
	
	@Autowired
	UserRepository userRepository;
	
	
	public static void main(String[] args) {
		SpringApplication.run(AuthenticationServiceApplication.class, args);
	}


	@Override
	public void run(String... args) throws Exception {
		Resource resource = resourceLoader.getResource("classpath:managers.csv");
		try(BufferedReader br = new BufferedReader(new InputStreamReader(resource.getInputStream()))){
			String managerToBeAdded = "";
			managerToBeAdded = br.readLine();
			while(managerToBeAdded != null) {
				String[] managerDetails = managerToBeAdded.split(",");
				User manager = new User();
				manager.setId(Integer.parseInt(managerDetails[0]));
				manager.setCustId(managerDetails[1]);
				manager.setEmail(managerDetails[2]);
				manager.setUsername(managerDetails[3]);
				manager.setPassword(managerDetails[4]);
				manager.setRole(managerDetails[5]);
				System.out.println("Manager" + manager);
				/*Check whether the manager is already existed or not,
				 *Managers.csv file will be read everytime you restart the service and will by default store
				 *the data into the database so first check if the manager already created or not*/
				User existManager = userRepository.findByEmailAndPassword(manager.getEmail(), manager.getPassword());
				if(existManager== null)
				userService.saveUser(manager);
				managerToBeAdded = br.readLine();
			}
		
		}
	}

}
